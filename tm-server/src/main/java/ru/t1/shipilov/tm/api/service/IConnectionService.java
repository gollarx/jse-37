package ru.t1.shipilov.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();
}
