package ru.t1.shipilov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shipilov.tm.api.endpoint.*;
import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.api.repository.ISessionRepository;
import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.api.repository.IUserRepository;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.endpoint.*;
import ru.t1.shipilov.tm.repository.ProjectRepository;
import ru.t1.shipilov.tm.repository.SessionRepository;
import ru.t1.shipilov.tm.repository.TaskRepository;
import ru.t1.shipilov.tm.repository.UserRepository;
import ru.t1.shipilov.tm.service.*;
import ru.t1.shipilov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, connectionService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository, connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, connectionService);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService, connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
