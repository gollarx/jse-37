package ru.t1.shipilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws Exception;

    @NotNull
    User add(@Nullable User user) throws Exception ;

    @Nullable
    User findById(@Nullable String Id) throws Exception;

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @Nullable
    User removeById(@Nullable String Id) throws Exception;

    @Nullable
    User removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExist(@Nullable String email) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

}
